package repositories;

import domain.EnumerationValue;

public interface EnumerationValueRepository extends Repository {
	

	public EnumerationValue withName (String name);
	public EnumerationValue withIntKey (int key, String name);
	public EnumerationValue withStringKey (String key, String name);

}
